# App for Squats Profile View

## You can
1. Create Profile.
2. And can Edit Profile Inline
3. API calls are cached using redis.
4. Preview Profile

----
## For Installing it locally
*  Create Virtual Environment

> virtualenv squats_env

> source squats_env/bin/activate

* Clone the Repository

> git clone https://richarupela@bitbucket.org/richarupela/squatsprofile.git

> cd squatsprofile

* Install Requirements

> pip install -r requirements.txt

* Do

> ./manage.py migrate

> ./manage.py collectstatic

> ./manage.py runserver

You can see it working on localhost:8000
