from rest_framework import serializers
from taggit_serializer.serializers import (TagListSerializerField, TaggitSerializer)

from squats.models import Profile

class ProfileSerializer(TaggitSerializer, serializers.ModelSerializer):
    skills = TagListSerializerField()
    class Meta:
        model = Profile
        fields = ('name', 'age', 'skills', 'image1', 'image2', 'profile_image')
