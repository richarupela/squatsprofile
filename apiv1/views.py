from PIL import Image, ImageChops

from django.core.cache import cache
from django import forms
from django.http import Http404

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from squats.utils import _generate_cache_key, image_validation
from squats.models import Profile
from .serializers import ProfileSerializer


class ProfileAPIView(APIView):
    allowed_methods = ['POST', 'PUT', 'GET']
    serializer_class = ProfileSerializer

    def get_object(self, pk):
        try:
            return Profile.objects.get(pk=pk)
        except Profile.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        profile_id = pk
        profile = Profile.objects.get(pk=profile_id)
        serializer = ProfileSerializer(profile)

        # in memory cache
        cache_key = _generate_cache_key(request)
        response = Response(serializer.data)
        cache.set(cache_key, serializer.data)
        return response

    def post(self, request, pk):
        cache_key = _generate_cache_key(request)
        cache.delete(cache_key)
        profile_id = pk
        field = request.data.get('name')
        value = request.data.get('value')
        try:
            profile = self.get_object(profile_id)
            data = {}
            if request.data.get('image1'):
                image = image_validation(request.data.get('image1'))
                profile.image1 = image
                img1 = Image.open(request.data.get('image1'))
                img2 = Image.open(profile.image2)
                if ImageChops.difference(img1, img2).getbbox() is not None:
                    raise forms.ValidationError("The two image uploaded should be similar")
                profile.save()
                data['image1'] = profile.image1.url
            if request.data.get('image2'):
                # profile.image2 = request.data.get('image2')
                image = image_validation(request.data.get('image2'))
                profile.image2 = image
                img1 = Image.open(profile.image1)
                img2 = Image.open(request.data.get('image2'))
                if ImageChops.difference(img1, img2).getbbox() is not None:
                    raise forms.ValidationError("The two image uploaded should be similar")
                profile.save()
                data['image2'] = profile.image2.url
            if field:
                if not field == 'skills':
                    # set field value
                    setattr(profile, field, value)
                elif field == 'skills':
                    skills = value
                    if skills:
                        profile.skills.clear()
                        for skill in skills.split(','):
                            skill = skill.strip()
                            profile.skills.add(skill)
            profile.save()
            return Response(data, status=status.HTTP_200_OK)
        except forms.ValidationError as e:
            return Response({'error': e.message}, status=status.HTTP_400_BAD_REQUEST)
        except ValueError as e:
            return Response({'error': e.message}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print (e)
            return Response(status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, *args, **kwargs):
        profile_id = request.data.get('pk')
        profile = self.get_object(profile_id)
        serializer = ProfileSerializer(profile, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
