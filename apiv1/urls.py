from django.conf.urls import url

from .views import ProfileAPIView

urlpatterns = [
    url(r'^user/(?P<pk>\d+)/$', ProfileAPIView.as_view(), name='update_profile'),
]