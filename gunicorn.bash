#!/bin/bash

NAME="squats"                                  # Name of the application
DJANGODIR=/home/richa/djangoprojects/squatsprofile            # Django project directory
SOCKFILE=/home/richa/djangoprojects/squatsprofile/run/gunicorn.sock  # we will communicte using this unix socket
USER=richa                                      # the user to run as
NUM_WORKERS=3                                    # how many worker processes should Gunicorn spawn
#DJANGO_SETTINGS_MODULE=squats.local_settings             # which settings file should Django use
DJANGO_WSGI_MODULE=squatsprofile.wsgi                     # WSGI module name
ERROR_LOG_FILE=/home/richa/djangoprojects/logs/gunicorn_error.log  # gunicorn error log

echo "Starting $NAME as `whoami`"

# Activate the virtual environment
cd $DJANGODIR
source /home/richa/djangoprojects/squats_env/bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR

# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec /home/richa/djangoprojects/squats_env/bin/gunicorn ${DJANGO_WSGI_MODULE}:application \
  --name $NAME \
  --workers $NUM_WORKERS \
  --user=$USER \
  --bind=0.0.0.0:8000 \
  --log-level=debug \
  --timeout 1200 \
  # --error $ERROR_LOG_FILE

#  --bind=unix:$SOCKFILE