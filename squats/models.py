# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from taggit_autosuggest.managers import TaggableManager
from django.core.validators import MaxValueValidator
# Create your models here.

class Profile(models.Model):
	name = models.CharField(max_length=100)
	age = models.IntegerField(validators=[MaxValueValidator(150),])
	skills = TaggableManager(blank=True)
	image1 = models.FileField(upload_to='media/')
	image2 = models.FileField(upload_to='media/')
	PROFILE_IMAGE_CHOICES = (
		('dp1', "image1"),
		('dp2', "image2"),
	)
	profile_image = models.CharField(max_length=10,choices=PROFILE_IMAGE_CHOICES)
