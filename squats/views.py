# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic import FormView
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.core.urlresolvers import reverse

from .forms import UserProfileForm
from .models import Profile


class IndexView(ListView):
    template_name = 'index.html'
    paginate_by = 20
    model = Profile


class CreateUserProfileView(FormView):
    form_class = UserProfileForm
    template_name = "user_profile_form.html"

    def form_valid(self, form):
        if self.request.method == 'POST':
            form = UserProfileForm(self.request.POST, self.request.FILES)
            if form.is_valid():
                profile_obj = form.save()
                return HttpResponseRedirect(reverse('profile:display_user_profile', kwargs={'pk': profile_obj.pk}))
        else:
            form = UserProfileForm()
        return render(self.request, 'user_profile_form.html', {'form': UserProfileForm})


class EditUserProfileView(DetailView):
    model = Profile
    template_name = "edit_user_profile.html"

    def get_context_data(self, **kwargs):
        context = super(EditUserProfileView, self).get_context_data(**kwargs)
        user = get_object_or_404(Profile, pk=self.kwargs['pk'])
        if user.profile_image == 'dp1':
            context['dp_path'] = user.image1.url
        elif user.profile_image == 'dp2':
            context['dp_path'] = user.image2.url
        context['user'] = user
        return context

    def get_success_url(self, *args, **kwargs):
        return reverse('display-user-profile', kwargs={'pk': self.kwargs['pk']})


class DisplayUserProfileView(DetailView):
    model = Profile
    template_name = "user_profile.html"

    def get_context_data(self, **kwargs):
        context = super(DisplayUserProfileView, self).get_context_data(**kwargs)
        user = get_object_or_404(Profile, pk=self.kwargs['pk'])
        if user.profile_image == 'dp1':
            context['dp_path'] = user.image1.url
        elif user.profile_image == 'dp2':
            context['dp_path'] = user.image2.url
        context['user'] = user
        return context
