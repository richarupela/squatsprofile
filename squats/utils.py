import hashlib
import StringIO

from PIL import Image, ImageChops

from django import forms
from django.utils.encoding import force_bytes, iri_to_uri
from django.utils.cache import _i18n_cache_key_suffix
from django.core.files.uploadedfile import InMemoryUploadedFile


def _generate_cache_key(request, path=None):
        key_prefix = "user-profile-"
        if not path:
            url = hashlib.md5(force_bytes(iri_to_uri(request._request.path)))
        else:
           url =  hashlib.md5(force_bytes(iri_to_uri(request._request.path.build_absolute_uri(path))))
        cache_key = '%s.%s' % (
            key_prefix, url.hexdigest())
        return _i18n_cache_key_suffix(request, cache_key)


def image_validation(image):
    img = Image.open(image)
    w, h = img.size

    # validate dimensions
    min_width = 1280
    min_height = 960
    if w < min_width or h < min_height:
        raise forms.ValidationError('Please use an image that is greater or equal to '
              '%s x %s pixels.' % (min_width, min_height))

    if w > min_width or h > min_height:
        image = img.resize((min_width, min_height), Image.ANTIALIAS)
        image = pil_to_django(image, 'image.jpeg')
    return image


def pil_to_django(image, name, format="JPEG"):
    fobject = StringIO.StringIO()
    if image.mode != "RGB":
        image = image.convert("RGB")
    image.save(fobject, format=format)
    if name.startswith('/'):
        name = 'image.jpeg'
    thumb_file = InMemoryUploadedFile(fobject, None, name, 'image/jpeg',
                                      fobject.len, None)
    return thumb_file
