import StringIO

from PIL import Image, ImageChops
from imagekit.processors import ResizeToFill

from django import forms
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _
from django.core.files.uploadedfile import InMemoryUploadedFile

from .utils import pil_to_django, image_validation
from .models import Profile


class UserProfileForm(forms.ModelForm):
    PROFILE_IMAGE_CHOICES = (
        ('dp1', "image1"),
        ('dp2', "image2"),
    )
    profile_image = forms.ChoiceField(widget=forms.RadioSelect, choices=PROFILE_IMAGE_CHOICES)

    def save(self, commit=True, *args, **kwargs):
        user_profile = super(UserProfileForm, self).save(commit=False)
        user_profile.save()
        for item in self.cleaned_data.get('skills'):
            user_profile.skills.add(item)
        user_profile.save()
        return user_profile

    def clean_image1(self):
        image = self.cleaned_data.get('image1', False)

        if image:
            image = image_validation(image)
        return image

    def clean_image2(self):
        image = self.cleaned_data.get('image2', False)

        if image:
            image = image_validation(image)
        return image

    def clean(self):
        image1 = self.cleaned_data.get('image1', None)
        image2 = self.cleaned_data.get('image2', None)
        if image1 and image2:
            img1 = Image.open(image1)
            img2 = Image.open(image2)
            if ImageChops.difference(img1, img2).getbbox() is not None:
                raise forms.ValidationError(_("The two image uploaded should be similar"))
        return self.cleaned_data

    class Meta:
        model = Profile
        fields = ('name', 'age', 'skills', 'image1', 'image2', 'profile_image')
    
