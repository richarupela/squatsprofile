from django.conf.urls import include, url
from .views import CreateUserProfileView, EditUserProfileView, DisplayUserProfileView, IndexView

urlpatterns = [
    url(r'^$', IndexView.as_view(), name="index"),
    url(r'^profile/new/$', CreateUserProfileView.as_view(), name="new-user-profile"),
    url(r'^users/(?P<pk>\d+)/$', DisplayUserProfileView.as_view(), name="display_user_profile"),
    url(r'^users/(?P<pk>\d+)/edit/$', EditUserProfileView.as_view(), name="edit-user-profile"),
]